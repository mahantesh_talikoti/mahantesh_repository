package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name="employee_info")  
public class Employee_info extends Model{
	private static final long serialVersionUID = 1L;
	
	@Id
	public Integer id;
	
	public String firstname;
	public String lastname;
	
	public final Integer getId() {
		return id;
	}
	public final void setId(Integer id) {
		this.id = id;
	}
	public final String getFirstname() {
		return firstname;
	}
	public final void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public final String getLastname() {
		return lastname;
	}
	public final void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public Employee_info(String fname, String lname) {
		this.firstname = fname;
		this.lastname = lname;
	}
	
	public static Finder<Integer,Employee_info> find = new Finder<Integer,Employee_info>( Integer.class, Employee_info.class );
	
	public static  List<Employee_info> findAll(){
        return  Employee_info.find.orderBy("id").findList();
    }
	
	public static void delete(Integer id) {
		find.ref(id).delete();	
	}
	
	public static void create(Employee_info object){
		object.save();
	}
}
