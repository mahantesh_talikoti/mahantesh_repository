package controllers;

import java.util.List;

import models.Employee_info;
import play.*;
import play.mvc.*;
import views.html.*;

public class Application extends Controller {
	static int rowCount = Employee_info.find.findRowCount();
	
    public static Result index() {
        //return ok(index.render("Your new application is ready."));
        return ok(index.render("Hello World!!"));
    	
    }
    
    public static Result rowCount() {
        //return ok(index.render("Your new application is ready."));
        //return ok(index.render("Hello World!!"));
    	 Result res = ok(index.render("Number of records in Employee_info table: " + rowCount));
    	 //res += ok(index.render("" + rowCount));
         return res;
    }
    
    public static Result list(){
        List<Employee_info> employees = Employee_info.findAll();
        return ok(play.libs.Json.toJson(employees));
    }

}
