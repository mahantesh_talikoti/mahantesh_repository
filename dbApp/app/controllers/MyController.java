package controllers;

import models.Employee_info;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;

public class MyController extends Controller {
	public static Result index() {
        return ok(index.render("Hello from MyController!!"));
		//return TODO;
    }
	
	public static Result deleteTask(Integer id) {
		Employee_info.delete(id); 
	    return ok(index.render("Record Deleted"));
	}
	
	public static Result createRecord(String fname, String lname){
		Employee_info object = new Employee_info(fname,lname);
		Employee_info.create(object);
		return ok(index.render("Record Created"));
	}
}
